/**
 * third party libraries
 */
const bodyParser = require("body-parser");
const express = require("express");
const helmet = require("helmet");
const http = require("http");
const mapRoutes = require("express-routes-mapper");
const cors = require("cors");

const database = require("../config/database");

/* config */
const config = require("../config/");
const dbService = require("./services/db.service");
const auth = require("./policies/auth.policy");

// environment: development, staging, testing, production
const environment = process.env.NODE_ENV;

/**
 * express application
 */
const app = express();
const server = http.Server(app);
const mappedOpenRoutes = mapRoutes(config.publicRoutes, "app/controllers/");
const mappedAuthRoutes = mapRoutes(config.privateRoutes, "app/controllers/");
const DB = dbService(database, environment, config.migrate).start();

// allow cross origin requests
// configure to only allow requests from certain origins
app.use(cors());

// secure express app
app.use(
  helmet({
    dnsPrefetchControl: false,
    frameguard: false,
    ieNoOpen: false
  })
);

// parsing the request bodys
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use("/uploads", express.static("uploads"));
const multer = require("multer");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "./uploads/");
  },
  filename: (req, file, cb) => {
    cb(null, new Date().getTime() + file.originalname);
  }
});

const upload = multer({ storage: storage });

/* secure public routes with recaptcha */
// app.use(
//   /^\/public\/(register|login|contactus|cboxlogin|forgot-password|(email\/resend))$/,
//   (req, res, next) => recaptcha(req, res, next),
// );

// secure your private routes with jwt authentication middleware
app.all("/private/*", (req, res, next) => auth(req, res, next));
app.use("/private/user/add-category", upload.single("file"));
app.use("/private/user/update-profile", upload.single("file"));

// fill routes for express appliction
app.use("/public", mappedOpenRoutes);
app.use("/private", mappedAuthRoutes);

server.listen(config.port, () => {
  if (
    environment !== "production" &&
    environment !== "development" &&
    environment !== "testing"
  ) {
    console.error(
      `NODE_ENV is set to ${environment}, but only production and development are valid.`
    );
    process.exit(1);
  }
  return DB;
});

process.on("uncaughtException", err => {
  console.log(err);
  database.close();
  server.close(error => {
    console.log(error);
    if (error) return process.exit(1);
    return process.exit();
  });
  setTimeout(async () => {
    database.close();
    process.exit(1);
  }, 1500);
});

process.on("unhandledRejection", err => {
  console.log(err);
  database.close();
  server.close(error => {
    console.log(error);
    if (error) return process.exit(1);
    return process.exit();
  });
  setTimeout(async () => {
    database.close();
    process.exit();
  }, 1500);
});
