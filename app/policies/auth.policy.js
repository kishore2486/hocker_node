// usually: "Authorization: Bearer [token]" or "token: [token]"
const jwtService = require('../services/jwt.service');

module.exports = async (req, res, next) => {
  let tokenToVerify;
  if (req.header('Authorization')) {
    const parts = req.header('Authorization').split(' ');

    if (parts.length === 2) {
      const scheme = parts[0];
      const credentials = parts[1];

      if (/^Bearer$/.test(scheme)) {
        tokenToVerify = credentials;
      } else {
        return res.status(401).json({ msg: 'Format for Authorization: Bearer [token]' });
      }
    } else {
      return res.status(401).json({ msg: 'Format for Authorization: Bearer [token]' });
    }
  } else {
    return res.status(401).json({ msg: 'No Authorization was found' });
  }
  try {
    const token = jwtService().Verify(tokenToVerify);
    req.token = token;
    return next();
  } catch (error) {
    return res.status(401).json({
      status: 'invalid token',
    });
  }
};
