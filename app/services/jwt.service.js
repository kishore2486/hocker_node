const jwt = require('jsonwebtoken');
require('dotenv').config({
  path: `.env.${process.env.NODE_ENV}`,
});

const secretKey = process.env.SECRET_KEY || '@HOOKER@';

const jwtService = () => {
  const generate = (data) => jwt.sign(
    {
      data,
    },
    secretKey,
    { expiresIn: '7d' },
  );

  const Verify = (token) => jwt.verify(token, secretKey);

  return {
    generate,
    Verify,
  };
};

module.exports = jwtService;
