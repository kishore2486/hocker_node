const nodemailer = require('nodemailer');

const EmailConfig = {
  host: 'smtp.gmail.com',
  port: 465,
  secure: true, // use SSL
  auth: {
    user: process.env.EMAIL_USER,
    pass: process.env.EMAIL_PASS,
  },
};

const transporter = nodemailer.createTransport(EmailConfig);

const emailService = async (options) => {
  const mailOptions = {
    from: 'no-reply@hocker.com',
    ...options,
  };
  try {
    await transporter.sendMail(mailOptions);
    transporter.close();
    return true;
  } catch (error) {
    console.log(error);
    return transporter.verify((checkError) => new Error(checkError));
  }
};

module.exports = emailService;
