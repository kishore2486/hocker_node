/* eslint-disable */
const Detail = require("../models/Details");
const User = require("../models/User");
const Ratings = require("../models/Ratings");
const Categories = require("../models/Categories");
const Offers = require("../models/Offers");

const DetailController = () => {
  const addDetails = async (req, res) => {
    const { location, email } = req.body;
    const {
      data: { id: user_id, business_type }
    } = req.token;
    if (business_type) {
      if ((location || email) && user_id) {
        try {
          if (email) await User.update({ email }, { where: { id: user_id } });
          const result = await Detail.findOrCreate({
            where: { user_id },
            defaults: { location, user_id }
          });
          if (!result[1] && location) {
            await Detail.update({ location }, { where: { user_id } });
            return res.status(200).json({ msg: "updated!", error: false });
          }
          return res.status(200).json({ msg: "success", error: false });
        } catch (error) {
          return res.status(500).json({ msg: "Internel server error" });
        }
      } else {
        return res
          .status(400)
          .json({ error: "provide location or image any data" });
      }
    }
    return res.status(400).json({ msg: "Permission Denied!" });
  };

  const updateProfilePic = async (req, res) => {
    const image = req.file.path;
    const {
      data: { id: user_id }
    } = req.token;
    if (image) {
      try {
        const image_url = `http://localhost:2019/${image}`;
        const result = await Detail.findOrCreate({
          where: { user_id },
          defaults: { image_url }
        });
        if (!result[1]) {
          Detail.update({ image_url }, { where: { user_id } });
        }
        return result && res.status(200).json({ msg: "Successfully Updated!" });
      } catch (error) {
        return res.status(500).json({ msg: "Internel server error" });
      }
    }
    return res.status(400).json({ msg: "Please provied Image!" });
  };

  const getAllShops = async (req, res) => {
    const { location } = req.body;
    if (location) {
      const userLocations = await Detail.findAll({
        attributes: ["location", "image_url"],
        include: [
          {
            model: User,
            attributes: ["id", "email", "name", "business_type"],
            include: [
              { model: Ratings, attributes: ["user_rated_id", "rating"] }
            ]
          }
        ]
      });
      if (userLocations) {
        const applyFilter = userLocations.filter(result => {
          const regex = new RegExp(location, "gi");
          return result.location.match(regex);
        });
        const data =
          applyFilter.length > 0 &&
          applyFilter.reduce((a, c) => {
            // eslint-disable-next-line no-param-reassign
            a[c.User.business_type] = a[c.User.business_type] || [];
            a[c.User.business_type].push(c);
            return a;
          }, {});
        return res.status(200).json({ data: data || {} });
      }
      return res.status(200).json({ msg: "Not found any shops in this Shops" });
    }
    return res.status(400).json({ msg: "location not provided!" });
  };

  const getShopDetails = async (req, res) => {
    const { id } = req.params;
    if (id) {
      const Details = await Detail.findAll({
        where: { user_id: id },
        attributes: ["user_id", "location", "image_url"],
        include: [
          {
            model: User,
            attributes: ["name", "business_type"],
            include: [
              {
                model: Categories,
                attributes: [
                  "id",
                  "category_name",
                  "item_name",
                  "price",
                  "image_url"
                ]
              },
              {
                model: Ratings,
                attributes: ["user_rated_id", "rating", "reviews"],
                include: { model: User, attributes: ["name"] }
              }
            ]
          }
        ]
      });
      return res.status(200).json({ data: Details });
    }
    return res.status(400).json({ msg: "Id not provided!" });
  };

  const getOffers = async (req, res) => {
    const {
      data: { id: user_id, business_type }
    } = req.token;
    if (business_type) {
      const result = await Offers.findAll({ where: { user_id } });
      return res.status(200).json({ data: result });
    }
    return res.status(400).json({ msg: "Permission Denied!" });
  };

  return {
    addDetails,
    getAllShops,
    getShopDetails,
    updateProfilePic,
    getOffers
  };
};

module.exports = DetailController;
