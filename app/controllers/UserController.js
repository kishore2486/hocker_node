const User = require('../models/User');
const Rating = require('../models/Ratings');
const bcryptService = require('../services/bcrypt.service');
const jwtService = require('../services/jwt.service');
// const sendMail = require('../services/email.service');
// const verifyEmailTemplate = require('../../html-content/verify');

const UserController = () => {
  const signUp = async (req, res) => {
    const { email, name, password } = req.body;
    if (email && name && password) {
      try {
        const user = await User.create(req.body);
        // const token = jwtService().generate(user.id);
        /* trigger Email Start */
        // const verifyLink = `${process.env.VERIFY_URL}/${token}`;
        // const html = verifyEmailTemplate(name, verifyLink);
        // sendMail({
        //   to: email,
        //   subject: 'Hocker.com - Email Verification',
        //   html,
        // });
        /* trigger Email End */
        return (
          user
          && res.status(200).json({ status: 'You have successfully registered!' })
        );
      } catch (err) {
        if (err.errors && err.errors.length > 0) {
          const errors = err.errors.map((er) => er.message);
          return res.status(400).json({ errors });
        }
        return res.status(500).json({ msg: 'Internal server error' });
      }
    }
    return res.status(400).json({ errors: ['email or password has problem'] });
  };

  const verify = async (req, res) => {
    const { token } = req.body;
    if (token) {
      const { data: id } = jwtService().Verify(token);
      if (id) {
        try {
          const result = await User.update(
            { is_verified: 1 },
            { where: { id } },
          );
          if (result) {
            return res.status(200).json({ msg: 'verified', error: false });
          }
          return res.status(404).json({ msg: 'Not found', error: false });
        } catch (error) {
          return res.status(500).json({ msg: 'Internal server error' });
        }
      }
      return res.status(400).json({ msg: 'link expired!', error: true });
    }
    return res.status(400).json({ msg: 'token not provided!' });
  };

  const resendVerification = async (req, res) => {
    const { email } = req.body;
    if (email) {
      try {
        const result = await User.findOne({ where: { email } });
        if (result) {
          // const token = jwtService().generate(result.uuid);
          /* trigger Email Start */
          // const verifyLink = `${process.env.VERIFY_URL}/${token}`;
          // const html = verifyEmailTemplate(name, verifyLink);
          // sendMail({
          //   to: email,
          //   subject: 'Hocker.com - Email Verification',
          //   html,
          // });
          /* trigger Email End */
          return res.status(200).json({ msg: 'success', error: false });
        }
        return res.status(404).json({ msg: 'Email not found!', error: true });
      } catch (error) {
        return res.status(500).json({ msg: 'Internal server error' });
      }
    }
    return res.status(404).json({ msg: 'email not provided!' });
  };

  const login = async (req, res) => {
    const { email, password } = req.body;
    if (email !== '' && password !== '') {
      try {
        const user = await User.findOne({
          where: {
            email,
          },
        });
        if (!user) {
          return res.status(400).json({ msg: 'User not found' });
        }
        if (user && !user.is_active) {
          return res
            .status(400)
            .json({ msg: 'Oops! Your account has been banned/disabled.' });
        }
        if (bcryptService().comparePassword(password, user.password)) {
          if (user.is_verified) {
            const token = jwtService().generate(user);
            return (
              token
              && res.status(200).json({
                token,
                id: user.id,
                name: user.name,
                email: user.email,
                business_type: user.business_type,
                msg: 'success',
              })
            );
          }
          return res.status(401).json({ msg: 'Email not verified' });
        }
        return res.status(401).json({ msg: 'Invalid Password' });
      } catch (err) {
        console.log(err);
        return res.status(500).json({ msg: 'Internal server error' });
      }
    }
    return res.status(400).json({ msg: 'Email or password is wrong' });
  };

  const updatePassword = async (req, res) => {
    const { old_password, new_password } = req.body;
    const {
      data: { id },
    } = req.token;
    if (old_password && new_password) {
      if (old_password === new_password) {
        return res
          .status(400)
          .json({ msg: 'Old password and New password should not be same!' });
      }
      try {
        const user = await User.findOne({ where: { id } });
        if (bcryptService().comparePassword(old_password, user.password)) {
          await User.update(
            { password: bcryptService().password(new_password) },
            { where: { id } },
          );
          return res
            .status(200)
            .json({ msg: 'password updated!', error: false });
        }
        return res
          .status(404)
          .json({ msg: 'Old password is invalid!', error: true });
      } catch (error) {
        return res.status(500).json({ msg: 'Internal server error' });
      }
    }
    return res
      .status(400)
      .json({ msg: 'old and new password should be provide!' });
  };

  const userRating = async (req, res) => {
    const { rating, review } = req.body;
    const {
      data: { id: user_rated_id },
    } = req.token;
    const { user_id } = req.params;
    console.log(req.params);
    if ((rating || review) && user_id && user_rated_id) {
      try {
        await Rating.create({
          user_id,
          user_rated_id,
          rating: rating || null,
          reviews: review || null,
        });
        return res.status(200).json({ msg: 'success', error: false });
      } catch (error) {
        console.log(error);
        return res.status(500).json({ msg: 'Internal server error' });
      }
    }
    return res
      .status(400)
      .json({ msg: 'rating, user_id or user_rated_id not provided!' });
  };

  return {
    signUp,
    login,
    verify,
    resendVerification,
    updatePassword,
    userRating,
  };
};

module.exports = UserController;
