const Category = require('../models/Categories');
const Offers = require('../models/Offers');

const BusinessController = () => {
  const addCategory = async (req, res) => {
    const { category_name, item_name, price } = req.body;
    const {
      data: { id: user_id, business_type },
    } = req.token;
    const image_url = req.file ? `http://localhost:2019/${req.file.path}` : null;
    if (business_type) {
      if (category_name && item_name && price) {
        try {
          await Category.create({ ...req.body, image_url, user_id });
          return res.status(200).json({ msg: 'success', error: false });
        } catch (err) {
          if (err.errors && err.errors.length > 0) {
            const errors = err.errors.map((er) => er.message);
            return res.status(400).json({ errors });
          }
          return res.status(500).json({ msg: 'Internal server error' });
        }
      } else {
        return res.status(406).json({ msg: 'Category or Item name not provided!' });
      }
    }
    return res.status(200).json({ msg: 'Permission Denied!' });
  };

  const getCategories = async (req, res) => {
    const {
      data: { id: user_id, business_type },
    } = req.token;
    if (business_type) {
      try {
        const categories = await Category.findAll({
          where: { user_id },
          order: [['category_name', 'ASC']],
          attributes: ['id', 'user_id', 'category_name', 'item_name', 'image_url', 'price'],
        });
        let result;
        if (categories) {
          result = categories.reduce((r, a) => {
            // eslint-disable-next-line no-param-reassign
            r[a.category_name] = r[a.category_name] || [];
            r[a.category_name].push(a);
            return r;
          }, {});
        }
        return res.status(200).json({ categories: result, error: false });
      } catch (error) {
        return res.status(500).json({ msg: 'Internal server error' });
      }
    }
    return res.status(200).json({ msg: 'Permission Denied!' });
  };

  const deleteCategory = async (req, res) => {
    const { category_name } = req.body;
    const {
      data: { id: user_id, business_type },
    } = req.token;
    if (business_type) {
      if (category_name) {
        try {
          const result = await Category.destroy({
            where: { user_id, category_name },
          });
          if (result) {
            return res.status(200).json({ msg: 'success', error: false });
          }
          return res.status(200).json({ msg: 'Not found', error: true });
        } catch (error) {
          return res.status(500).json({ msg: 'Internal server error' });
        }
      }
      return res.status(406).json({ msg: 'Category name not provided!' });
    }
    return res.status(200).json({ msg: 'Permission Denied!' });
  };

  const deleteCategoryItem = async (req, res) => {
    const { category_name, item_name } = req.body;
    const {
      data: { id: user_id, business_type },
    } = req.token;
    if (business_type) {
      if (category_name && item_name) {
        try {
          const result = await Category.destroy({
            where: { user_id, category_name, item_name },
          });
          if (result) {
            return res.status(200).json({ msg: 'success', error: false });
          }
          return res.status(404).json({ msg: 'Not found', error: true });
        } catch (error) {
          return res.status(500).json({ msg: 'Internal Server error!' });
        }
      }
      return res.status(406).json({ msg: 'Category name or Item name not provided!' });
    }
    return res.status(200).json({ msg: 'Permission Denied!' });
  };

  const addOffers = async (req, res) => {
    const {
      data: { id: user_id, business_type },
    } = req.token;
    if (business_type) {
      try {
        await Offers.create({ ...req.body, user_id });
        return res.status(200).json({ msg: 'success', error: false });
      } catch (err) {
        if (err.errors && err.errors.length > 0) {
          const errors = err.errors.map((er) => er.message);
          return res.status(400).json({ errors });
        }
        return res.status(500).json({ msg: 'Internal server error' });
      }
    }
    return res.status(200).json({ msg: 'Permission Denied!' });
  };

  const deleteOffer = async (req, res) => {
    const { id } = req.body;
    const {
      data: { id: user_id, business_type },
    } = req.token;
    if (business_type) {
      if (id) {
        try {
          const result = await Offers.destroy({
            where: { user_id, id },
          });
          if (result) {
            return res.status(200).json({ msg: 'success', error: false });
          }
          return res.status(404).json({ msg: 'Not found', error: true });
        } catch (error) {
          return res.status(500).json({ msg: 'Internal server error' });
        }
      }
      return res.status(400).json({ msg: 'Id should be provide!' });
    }
    return res.status(200).json({ msg: 'Permission Denied!' });
  };

  return {
    addCategory,
    getCategories,
    deleteCategory,
    deleteCategoryItem,
    addOffers,
    deleteOffer,
  };
};

module.exports = BusinessController;
