const Sequelize = require('sequelize');
const User = require('./User');

const sequelize = require('../../config/database');

const tableName = 'details';

const Details = sequelize.define(
  'detail',
  {
    user_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
    },
    location: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: null,
    },
    image_url: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: null,
    },
  },
  { tableName, underscored: true },
);

Details.belongsTo(User);
User.hasMany(Details);

module.exports = Details;
