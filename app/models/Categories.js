const Sequelize = require('sequelize');
const User = require('./User');

const sequelize = require('../../config/database');

const tableName = 'categories';

const Categories = sequelize.define(
  'categorie',
  {
    user_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
    },
    category_name: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        len: {
          args: [3, 50],
          msg: 'Name should be atleast 3 characters long',
        },
      },
    },
    item_name: {
      type: Sequelize.STRING,
      allowNull: false,
      unique: {
        args: true,
        msg: 'Name already created!',
      },
      validate: {
        len: {
          args: [3, 50],
          msg: 'Name should be atleast 3 characters long',
        },
      },
    },
    price: {
      type: Sequelize.INTEGER,
      allowNull: false,
    },
    image_url: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: null,
    },
  },
  { tableName, underscored: true },
);

Categories.belongsTo(User);
User.hasMany(Categories);

module.exports = Categories;
