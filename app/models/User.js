const Sequelize = require('sequelize');
const bcryptService = require('../services/bcrypt.service');

const sequelize = require('../../config/database');

const hooks = {
  beforeCreate(user) {
    user.password = bcryptService().password(user); // eslint-disable-line no-param-reassign
  },
};

const tableName = 'users';

const User = sequelize.define(
  'User',
  {
    id: {
      type: Sequelize.UUID,
      allowNull: false,
      unique: true,
      defaultValue: Sequelize.UUIDV4,
      primaryKey: true,
    },
    email: {
      type: Sequelize.STRING,
      allowNull: false,
      unique: {
        args: true,
        msg: 'Email already registered.',
      },
      validate: {
        isEmail: {
          args: true,
          msg: 'Email invalid.',
        },
      },
    },
    name: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        len: {
          args: [3, 50],
          msg: 'Name should be atleast 3 characters long',
        },
      },
    },
    password: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        len: {
          args: [8, 80],
          msg: 'Password must be atleast 8 characters long',
        },
      },
    },
    business_type: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: null,
    },
    mobile: {
      type: Sequelize.INTEGER,
      allowNull: true,
      validate: {
        len: {
          args: 10,
          msg: 'Mobile Number must be 10 characters',
        },
      },
    },
    is_verified: {
      type: Sequelize.BOOLEAN,
      defaultValue: 1,
    },
    is_active: {
      type: Sequelize.BOOLEAN,
      defaultValue: 1,
    },
  },
  { hooks, tableName, underscored: true },
);

/* eslint-disable-next-line */
User.prototype.toJSON = function() {
  const {
 id, name, email, business_type 
} = this.get();
  return {
    id,
    name,
    email,
    business_type,
  };
};

module.exports = User;
