const Sequelize = require('sequelize');
const User = require('./User');

const sequelize = require('../../config/database');

const tableName = 'ratings';

const Ratings = sequelize.define(
  'rating',
  {
    user_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
    },
    user_rated_id: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    rating: {
      type: Sequelize.INTEGER,
      allowNull: true,
    },
    reviews: {
      type: Sequelize.STRING,
      allowNull: true,
    },
  },
  { tableName, underscored: true },
);

Ratings.belongsTo(User);
User.hasMany(Ratings);

module.exports = Ratings;
