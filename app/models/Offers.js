const Sequelize = require('sequelize');
const User = require('./User');

const sequelize = require('../../config/database');

const tableName = 'offers';

const Offers = sequelize.define(
  'offer',
  {
    user_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
    },
    name: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    discount: {
      type: Sequelize.INTEGER,
      allowNull: false,
    },
    on_above_price: {
      type: Sequelize.INTEGER,
      allowNull: false,
    },
    expires: {
      type: Sequelize.DATE,
      allowNull: false,
    },
  },
  { tableName, underscored: true },
);

Offers.belongsTo(User);

module.exports = Offers;
