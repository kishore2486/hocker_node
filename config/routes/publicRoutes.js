const publicRoutes = {
  'POST /user/signup': 'UserController.signUp',
  'POST /user/login': 'UserController.login',
  'POST /user/verify': 'UserController.verify',
  'POST /user/resend-verification': 'UserController.resendVerification',
  'POST /user/get-all-shops': 'DetailController.getAllShops',
  'GET /user/get-shop-details/:id': 'DetailController.getShopDetails',
};

module.exports = publicRoutes;
