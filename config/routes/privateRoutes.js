const privateRoutes = {
  "POST /user/update-password": "UserController.updatePassword",
  "POST /user/add-details": "DetailController.addDetails",
  "POST /user/add-category": "BusinessController.addCategory",
  "GET /user/get-categories": "BusinessController.getCategories",
  "DELETE /user/delete-category": "BusinessController.deleteCategory",
  "DELETE /user/delete-category-item": "BusinessController.deleteCategoryItem",
  "POST /user/add-offers": "BusinessController.addOffers",
  "DELETE /user/delete-offer": "BusinessController.deleteOffer",
  "POST /user/user-rating/:user_id": "UserController.userRating",
  "POST /user/update-profile": "DetailController.updateProfilePic",
  "GET /user/get-offers": "DetailController.getOffers"
};

module.exports = privateRoutes;
